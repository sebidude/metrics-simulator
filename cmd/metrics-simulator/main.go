package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	log "log/slog"
	"net/http"
	"os"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	namespace     = "simlator"
	counterLabels = []string{"code", "method", "endpoint"}

	runCounter              *prometheus.CounterVec
	lastRunSuccessTimestamp *prometheus.GaugeVec
)

func updateRun() {

	go func() {
		for {

			code := "200"
			if time.Now().Second()%5 == 0 {
				code = "500"
			} else {
				code = "200"
			}
			runCounter.With(prometheus.Labels{"code": code, "method": http.MethodGet, "endpoint": "/foo"}).Inc()
			time.Sleep(time.Second)
		}
	}()
}

func recordSuccess(w http.ResponseWriter, r *http.Request) {
	values := r.URL.Query()
	job := values.Get("job")
	now := time.Now().Unix()
	lastRunSuccessTimestamp.With(prometheus.Labels{"job": job}).Set(float64(now))
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(fmt.Sprintf("set job %s to success at %d\n", job, now)))
}

func printAlert(w http.ResponseWriter, r *http.Request) {
	body, err := io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(500)
		w.Write(bytes.NewBufferString(err.Error()).Bytes())
		return
	}
	defer r.Body.Close()

	var data interface{}
	err = json.Unmarshal(body, &data)
	if err != nil {
		w.WriteHeader(500)
		w.Write(bytes.NewBufferString(err.Error()).Bytes())
		return
	}
	alert, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		w.WriteHeader(500)
		w.Write(bytes.NewBufferString(err.Error()).Bytes())
		return
	}
	w.WriteHeader(200)
	w.Write([]byte("OK"))
	fmt.Println(alert)
}

func main() {

	if len(os.Args) == 1 {
		fmt.Printf("usage: %s port [namespace]\n", os.Args[0])
		os.Exit(1)
	}

	logger := log.New(log.NewJSONHandler(os.Stdout, nil))
	if len(os.Args) > 2 {
		namespace = os.Args[2]
	}

	port := os.Args[1]

	runCounter = promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace: namespace,
		Subsystem: "run",
		Name:      "total",
	}, counterLabels)

	lastRunSuccessTimestamp = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Subsystem: "run",
		Name:      "success_timestamp",
	}, []string{"job"})
	logger.Info("metrics-simulator", "port", port, "namespace", namespace)
	updateRun()

	http.HandleFunc("/success", recordSuccess)
	http.Handle("/metrics", promhttp.Handler())
	http.HandleFunc("/alerts/", printAlert)
	http.ListenAndServe(fmt.Sprintf(":%s", port), nil)
}
